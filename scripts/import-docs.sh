#!/bin/bash

# In the past, our documentation was hosted at https://developer.gnome.org/NetworkManager/
# and https://developer.gnome.org/libnm/. But that tooling is no longer supported and
# recent documentation is no longer there.
#
# Instead, it's at our website, like
# https://networkmanager.dev/docs/api/latest/NetworkManager.html and
# https://networkmanager.dev/docs/api/1.34.0/NetworkManager.html
#
# Run this script to import a new documentation. It downloads the release
# tarball from https://gitlab.freedesktop.org/NetworkManager/NetworkManager/-/releases,
# which contains pre-generated docs. It then copies them to the versioned location.
#
# Pass "-l" if (and only if) the newly documentation is the latest version.
# That causes the docs to also be copied (and available) via a "latest" link,
# like https://networkmanager.dev/docs/api/latest/.
#
# By default, the script will also update the major alias, for example, importing
# https://networkmanager.dev/docs/api/1.42.2 will also create https://networkmanager.dev/docs/api/1.42.
# That is for importing the latest minor release of a stable branch. You can
# suppress that with "-L".
#
# Example:
#   $ ./scripts/import-docs.sh 1.34.0 -l
#   $ #... review
#   $ git push

set -e

die() {
    printf '%s\n' "$*" >&2
    exit 1
}

usage() {
    echo "$0 <VERSION> [ -l | -L ]"
    echo
    echo " By default, the script updates the major links like https://networkmanager.dev/docs/api/1.40/."
    echo " That can be suppressed by passing '-L'"
    echo " If '-l' is specified, the script also updates the latest links like https://networkmanager.dev/docs/api/latest."
    exit 0
}

ARGS=("$0" "$@")

CMD="${ARGS[*]}"

unset VERSION
DO_LATEST_MAJ=0
DO_LATEST_MIN=1
for a ; do
    case "$a" in
        -h)
            usage
            ;;
        -L)
            DO_LATEST_MAJ=0
            DO_LATEST_MIN=0
            ;;
        -l)
            DO_LATEST_MAJ=1
            DO_LATEST_MIN=1
            ;;
        *)
            test -z "${VERSION+x}" || die "Invalid argument \"$a\""
            VERSION="$1"
            ;;
    esac
done
test -n "${VERSION+x}" || usage

OLD_IFS="$IFS"
IFS=.
VERSION_ARRAY=( $VERSION )
IFS="$OLD_IFS"
VERSION_MIN=

case "${#VERSION_ARRAY[@]}" in
    2)
        test "$VERSION" == "${VERSION_ARRAY[0]}.${VERSION_ARRAY[1]}" || die "invalid version $VERSION"
        DO_LATEST_MIN=0
        ;;
    3)
        test "$VERSION" == "${VERSION_ARRAY[0]}.${VERSION_ARRAY[1]}.${VERSION_ARRAY[2]}" || die "invalid version $VERSION"
        VERSION_MIN="${VERSION_ARRAY[0]}.${VERSION_ARRAY[1]}"
        ;;
    4)
        test "$VERSION" == "${VERSION_ARRAY[0]}.${VERSION_ARRAY[1]}.${VERSION_ARRAY[2]}.${VERSION_ARRAY[3]}" || die "invalid version $VERSION"
        DO_LATEST_MIN=0
        ;;
    *)
        die "invalid version $VERSION"
        ;;
esac

FILENAME_BASE="NetworkManager-$VERSION"
FILENAME_TAR="$FILENAME_BASE.tar.xz"
FILENAME_CSUM="$FILENAME_TAR.sha256sum"

cd "$(git rev-parse --show-toplevel)"

download_tarball() {
    local TAR_URL="https://gitlab.freedesktop.org/NetworkManager/NetworkManager/-/releases/$VERSION/downloads/$FILENAME_TAR"
    local CSUM_URL="https://gitlab.freedesktop.org/NetworkManager/NetworkManager/-/releases/$VERSION/downloads/$FILENAME_CSUM"

    wget -O "./tmp/$FILENAME_TAR" "$TAR_URL" || die "failed to download tarball at $TAR_URL"
    wget -O "./tmp/$FILENAME_CSUM" "$CSUM_URL" || die "failed to download checksum file at $CSUM_URL"

    (cd ./tmp && sha256sum -c "./$FILENAME_CSUM") || die "invalid checksum $FILENAME_TAR"
}

extract_tarball() {
    rm -rf "./tmp/$FILENAME_BASE/" 2>/dev/null
    (cd ./tmp && tar -xvf "./$FILENAME_TAR") || die "failure to extract ./tmp/$FILENAME_TAR"
    DIRNAME="$FILENAME_BASE"
    if ! test -d "./tmp/$DIRNAME/"; then
        DIRNAME="$(printf '%s' "$DIRNAME" | sed 's/-rc[0-9]\+$//')"
        test -d "./tmp/$DIRNAME/" || die "extracted archive not found"
    fi
}

detect_docs() {
    local D
    local has=0

    for D in $(ls -1d "./tmp/$DIRNAME/docs"/*/html/ 2>/dev/null) ; do
        D="${D%/html/}"
        D="${D##*/}"
        printf '%s\n' "$D"
        has=1
    done
    if [ "$has" = 0 ] ; then
        printf '%s\n' "No docs found in ./tmp/$DIRNAME/docs/*/html/" >&2
        return 1
    fi
}

move_docs() {
    local D

    for D in "${DOC_DIRS[@]}" ; do
        mkdir -p "./static/docs/$D/"
        rm -rf "./static/docs/$D/$VERSION/"
        mv "./tmp/$DIRNAME/docs/$D/html/" "./static/docs/$D/$VERSION/"
        if [ "$DO_LATEST_MAJ" = 1 ] ; then
            rm -rf "./static/docs/$D/latest"
            cp -r "./static/docs/$D/$VERSION/" "./static/docs/$D/latest"
        fi
        if [ "$DO_LATEST_MIN" = 1 ] ; then
            rm -rf "./static/docs/$D/$VERSION_MIN"
            cp -r "./static/docs/$D/$VERSION/" "./static/docs/$D/$VERSION_MIN"
        fi
    done
}

commit() {
    local D

    for D in "${DOC_DIRS[@]}" ; do
        git add -f "./static/docs/$D/$VERSION/"
        if [ "$DO_LATEST_MAJ" = 1 ] ; then
            git add -f "./static/docs/$D/latest/"
        fi
        if [ "$DO_LATEST_MIN" = 1 ] ; then
            git add -f "./static/docs/$D/$VERSION_MIN/"
        fi
    done
    git commit -m "docs: import documentation from $FILENAME_TAR

sha256sum: $(sha256sum "./tmp/$FILENAME_TAR" | sed 's/^\([a-f0-9]\+\)[	 ]\+.*/\1/')

  $ $CMD"
}

mkdir -p ./tmp/

download_tarball
DIRNAME=
extract_tarball
DOC_DIRS=( $(detect_docs) ) || exit 0
move_docs
commit

rm -rf "./tmp/$FILENAME_TAR"
rm -rf "./tmp/$FILENAME_BASE/"
