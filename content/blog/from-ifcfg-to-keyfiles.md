+++
date = "2022-04-29T01:00:00+02:00"
title = "From ifcfg to keyfiles [link]"
tags = ["ifcfg-rh", "keyfile"]
draft = false
author = "Lubomir Rintel"
description = "Modernizing NetworkManager configuration in Fedora Linux 36"
weight = 10
+++

One of the changes in Fedora Linux 36 is that new installations will
no longer support the ifcfg files to configure networking. What are
those and what replaces them?

Read more on [Fedora Magazine](https://fedoramagazine.org/converting-networkmanager-from-ifcfg-to-keyfiles/)
