+++
date = "2022-02-24T12:00:00+01:00"
title = "NetworkManager 1.36"
tags = ["release"]
draft = false
author = "Thomas Haller"
description = "What's new"
weight = 10
+++

This is the announcement of major release NetworkManager 1.36.0 on 24th February 2022.
It comes six weeks after the previous major release.

![Sophie and Jirka, without NetworkManager](/blog/images/sophie_and_jirka.jpg "Sophie and Jirka, without NetworkManager")

See what is new.


## Rework IP Configuration

The way NetworkManager handles IP configuration internally was heavily reworked.
The goal was more maintainable and extendable code (see [here](https://gitlab.freedesktop.org/NetworkManager/NetworkManager/-/blob/d85b934370156496f169adab5322d38cb4bb419a/src/core/README.l3cfg.md)).

This was a huge rework, but the goal is that it has little visible impact to the user.
In the best case, everything still works the same -- or better. And as far as we know, it
does work well. If you want to be extra careful on your production system, this
is the reason to wait before upgrading just yet.


## Routing

NetworkManager now ignores routes of protocols that it doesn't support.
This is done to help with performance problems with a huge number of
routes, for example with BGP software.

NetworkManager now also supports blackhole, unreachable and prohibit
type routes.

Also it handles IPv6 multipath routes better. Previously, NetworkManager
didn't understand such routes, which could lead to wrong behavior.


## No More "configure-and-quit" Mode

NetworkManager had a mode where it would configure the network and quit.
The goal of that was to avoid a running daemon with static configuration.
This mode was dropped, as it was very little used
(see [mailing list](https://mail.gnome.org/archives/networkmanager-list/2021-September/msg00010.html)).


## Conclusions

See also our [NEWS](https://gitlab.freedesktop.org/NetworkManager/NetworkManager/-/blob/1.36.0/NEWS) for more details.

You can find the release tarball as usual on our [download page](https://download.gnome.org/sources/NetworkManager/1.36/).

If you are using Fedora/RHEL, you can try our [copr repositories](https://copr.fedorainfracloud.org/coprs/networkmanager/).

This release was possible thanks to the contributions of: Ana Cabral,
Andrew Zaborowski, Beniamino Galvani, Daniele Palmas, Fernando Fernandez
Mancera, James Hilliard, Justin Spencer, Lubomir Rintel, Nacho Barrientos,
Sam Morris, Thomas Haller, Tomohiro Mayama, Val Och, Wen Liang, xiangnian,
流浪猫.

As always, thanks to our Red Hat QA Vladimír Beneš, Filip Pokryvka,
David Jasa, Matej and Vitezslav Humpa.
