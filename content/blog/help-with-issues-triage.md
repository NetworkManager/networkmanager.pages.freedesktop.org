+++
date = "2024-06-20T08:00:00+00:00"
title = "Help NetworkManager project to triage issues"
tags = ["community"]
draft = false
author = "Íñigo Huguet"
description = "Know other ways to contribute in issues resolution other than code contributions"
weight = 10
+++

Some small modifications has arrived to our workflow to triage issues. We have
done this to encourage more people to get more involved in their resolution.

## Context

Despite NetworkManager being a very important tool on a really important number
of the Linux systems that are out there, all the development is done by a very
small team of developers with very few community contributions. This causes us
to rarely have enough time to dedicate to all the issues that are reported, so
they are accumulating without nobody actually working on them.

We acknowledge that the code has, at the very least, a high barrier entry due
to its complexity. We know that many people would like to contribute patches
but the difficulty is so high that the effort isn't worth it.

## Other ways to help

However, we can also see that among you there are people with great knowledge
about networking, many of them with a good knowledge about how to configure and
use NetworkManager. Even if not contributing with code, your help might be
really valuable to triage and investigate possible resolutions for the issues.
Once a NetworkManager's developer has a clear understanding of the problem and
some good ideas about how to solve it, the chances that he/she can prepare a
patch multiplies.

From now on, any open issue can be in one of three stages:
- Triage: decide whether the issue is a real bug or valid feature request, or
  if it should be closed.
- Investigation: think about how the issue can be solved or what the affected
  part of the code is. The goal is to gather enough info to allow the work to
  start.
- Development: fix the bug / implement the feature.

Triage and investigation are stages to which any people with knowledge in
networking can contribute, so we encourage you to use yours to assist us. Look
for issues with the labels [_help-needed::triage_ and _help-needed::investigation_](https://gitlab.freedesktop.org/NetworkManager/NetworkManager/-/issues/?sort=created_date&state=opened&or%5Blabel_name%5D%5B%5D=help-needed%3A%3Atriage&or%5Blabel_name%5D%5B%5D=help-needed%3A%3Ainvestigation&first_page_size=100)
in the issue tracker.

## Start helping with issues triage today

Read the detailed info in the repo's [CONTRIBUTING.md](https://gitlab.freedesktop.org/NetworkManager/NetworkManager/-/blob/main/CONTRIBUTING.md?ref_type=heads#help-with-issues-triage)
file.

Go to the [issue tracker](https://gitlab.freedesktop.org/NetworkManager/NetworkManager/-/issues)
and find issues to help with.

