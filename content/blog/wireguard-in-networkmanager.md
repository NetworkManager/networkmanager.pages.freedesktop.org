+++
date = "2019-03-15T12:00:00+01:00"
title = "WireGuard in NetworkManager [link]"
tags = ["wireguard"]
draft = false
author = "Thomas Haller"
description = "NetworkManager 1.16 got native support for WireGuard VPN tunnels"
weight = 10
+++

NetworkManager 1.16 got native support for WireGuard VPN
tunnels. WireGuard is a novel VPN tunnel protocol and implementation
that spawned a lot of interest.

Read more on [Thomas Haller's Blog](https://blogs.gnome.org/thaller/2019/03/15/wireguard-in-networkmanager/).


