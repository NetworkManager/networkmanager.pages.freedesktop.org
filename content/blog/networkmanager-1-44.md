+++
date = "2023-08-10T08:00:00+00:00"
title = "NetworkManager 1.44"
tags = ["release"]
draft = false
author = "Fernando F. Mancera"
description = "What's new"
weight = 10
+++

Half a year and 796 commits since NetworkManager 1.42, a new release
is ready: NetworkManager 1.44.

Let's take a look at the most interesting parts!

![Roque celebrates Pride month with NetworkManager](/blog/images/roque-pride.jpg "Roque celebrates Pride month with NetworkManager")

## The ifcfg-rh plugin is deprecated

The ifcfg-rh plugin is deprecated; it will only receive bugfixes and no new
features. A warning is emitted to the log when a connection in ifcfg-rh format
is found.

In addition, to automatically migrate existing ifcfg-rh connections to the
keyfile format, a new configuration option `main.migrate-ifcfg-rh` is provided.
Migration is disabled by default, but the default value can be changed at build
time via `--with-config-migrate-ifcfg-rh-default=yes`. We strongly recommend
all the packagers and distributions to enable this option by default as
ifcfg-rh is now deprecated.

## Disable Wi-Fi and WWAN radio in `nmtui`

`nmtui` is a screen-oriented configuration tool for NetworkManager. It is not
powerful as `nmcli` but it is very useful for non-command-line-tool oriented
users.

Now it is possible to disable Wi-Fi and WWAN radio using the `nmtui` client.

![nmtui](/blog/images/nmtui-disable-wifi.jpg "Disabling Wi-Fi and WWAN with nmtui")

## What else?

Now the number of autoconnect retries left is tracked for each device and
connection. Previously it was tracked only per connection and this lead to
unexpected behaviors in case of multiconnect profiles.

`nmcli` throws a warning when NetworkManager version mismatches.

A new `link` setting that holds properties related to the kernel link such as
`tx-queue-length`, `gso-max-size`, `gso-max-segments` and `gro-max-size` has
been introduced.

Plus a lot of bugfixes and translation updates. See
[NEWS](https://gitlab.freedesktop.org/NetworkManager/NetworkManager/-/blob/1.44.0/NEWS)
file for even more detailed summary of what's new.

## Acknowledgements

Many thanks to all contributors who provided feedback, ideas or patches.

Thomas Haller, Alexander Lochmann, Beniamino Galvani, Fernando Fernandez
Mancera, Yuri Chornoivan, Lubomir Rintel, Michael Biebl, Wen Liang, Dylan Van
Assche, Yaakov Selkowitz, Stewart Smith, Heiko Thiery, Julia Dronova, Ratchanan
Srirattanamet, Sven Schwermer, Fabrice Fontaine, NorwayFun, Etienne Champetier,
Corentin Noël, Vladislav Tsisyk, liaohanqin, Jordi Mas, Daniel Kolesa, Haochen
Tong, Joao Machado, Alfonso Sánchez-Beato, AsciiWolf, Peter Hutterer, Aleksandr
Melman, Eivind Næss, Marc Muehlfeld, Tom Sobczynski, Sabri Unal, David
Woodhouse, Benjamin Berg, Sabri Ünal, Frederic Martinsons, Petr Menšík, Gris
Ge, Javier Sánchez Parra, Miroslav Suchy, qyecst, chengyechun, Jan Vaclav,
Íñigo Huguet

Also thanks to our Quality Engineers from Red Hat for all the testing:
Vladimír Beneš, Filip Pokryvka, David Jasa and Matej Berezny.

Join us on our [GitLab project](https://gitlab.freedesktop.org/NetworkManager/NetworkManager/).

## Get the new release

As usual, the next release of your favorite Linux distribution will surely
ship the new version.

In case you're too impatient to wait, or you are, in fact, responsible
for keeping NetworkManager up to date in a distribution, get the tarball
from our [download page](https://download.gnome.org/sources/NetworkManager/1.44/).

Thanks for tuning in and goodbye!
