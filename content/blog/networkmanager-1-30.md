+++
date = "2021-02-18T12:00:00+01:00"
title = "NetworkManager 1.30"
tags = ["release", "veth", "wpa3", "initrd", "libnm"]
draft = false
author = "Beniamino Galvani"
description = "What's new"
weight = 10
+++

Two months and a half after the previous release, NetworkManager 1.30
has seen the light today. The development cycle was shorter compared
to past releases and therefore the list of new features is not as
exciting as usual. There is however interesting stuff worth
mentioning.

## Create your veth interfaces with NM

A virtual Ethernet (veth) is a special kind of network interface
supported by the Linux kernel. It is *virtual* because it doesn't
represent a hardware device. You can create veths only in pairs and
all the traffic sent through one interface is received by the other
veth.

Until now NM treated a veth interface like a normal Ethernet. It was
possible to activate a connection profile of type `ethernet` on an
existing veth and NM would bring the interface up and start DHCP (or
set up static addresses).

Users can now also create a veth pair through NetworkManager, by
adding a new connection profile of type `veth`. The connection needs a
`veth.peer` property that specifies the name of the peer interface to
create.

## Stronger security for WPA3-Enterprise

WPA3 (Wi-Fi Protected Access 3) is the latest version of the standard
developed by the Wi-Fi Alliance to secure wireless networks.

Among other things, it specifies a new authentication protocol (SAE,
Simultaneous Authentication of Equals) to be used in WPA-Personal
mode; NetworkManager already supports SAE since version 1.16.

Another addition of WPA3 is a new security level to be used with
WPA-Enterprise that guarantees higher security by only allowing
protocols with a minimum strength of 192 bits. This new WPA-Enterprise
192-bit mode can now be configured in NetworkManager by setting the
`wifi-security.key-mgmt` property to `WPA-EAP-SUITE-B-192`.

## initrd generator improvements

Since version 1.16, NetworkManager provides a connection generator
tool that parses the kernel command line in the initramfs and creates
a list of connection profiles that NetworkManager can activate during
early stages of the boot.

This tool has seen a lot of improvements recently, including many bug
fixes and new features. Among them, there is the support for the
`rd.net.timeout.carrier` and `rd.net.dhcp.retry` kernel command line
arguments. The generator now also accepts a new `link6`
autoconfiguration mode in `ip=` arguments to use only link-local IPv6
addressing.

## A new libnm API to read and write connection profiles

This item is particularly interesting for developers. libnm is a
library to interact with NetworkManager and provides an API based on
objects (Devices, Connections, Settings, IP Configurations, etc.) that
can be easily used to discover and change the network configuration,
hiding the complexity of the D-Bus API of NetworkManager.

In this new release, libnm provides functions to read and write
connection profiles in the keyfile format, the one used natively by
NetworkManager. Check the
[example](https://gitlab.freedesktop.org/NetworkManager/NetworkManager/-/blob/1.30.0/examples/python/gi/nm-keyfile.py)!

## Conclusion

This is an incomplete list of some of the changes introduced by the
new 1.30 release. For the full list please see the [NEWS
file](https://gitlab.freedesktop.org/NetworkManager/NetworkManager/-/blob/1.30.0/NEWS).


And, as always, thanks to everybody who contributed.
