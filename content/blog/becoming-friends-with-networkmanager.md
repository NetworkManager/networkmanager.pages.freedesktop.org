+++
date = "2019-08-05T12:00:00+01:00"
title = "Becoming friends with NetworkManager [link]"
tags = ["nmcli"]
draft = false
author = "Francesco Giudici"
description = "Need to configure a Linux machine with NetworkManager? Learn how, and why"
weight = 10
+++

Do you disable NetworkManager, and wonder why your preferred Linux
distro isn't using the old IP tools as the default network
configuration method? Do you think NetworkManager is "just for WiFi"?

This blog post is for you. Leave behind prejudice and read more on the
[Enable Sysadmin
Blog](https://www.redhat.com/sysadmin/becoming-friends-networkmanager).
