+++
date = "2022-09-16T01:00:00+02:00"
title = "Using Python and NetworkManager to control the network [link]"
tags = ["libnm", "python"]
draft = false
author = "Beniamino Galvani"
description = "Get started with some practical examples"
weight = 10
+++

This article first introduces the API of NetworkManager and presents
how to use it from a Python program. In the second part it shows some
practical examples: how to connect to a wireless network or to add an
IP address to an interface programmatically via NetworkManager.

Read more on [Fedora Magazine](https://fedoramagazine.org/using-python-and-networkmanager-to-control-the-network/)
