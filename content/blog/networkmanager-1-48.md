+++
date = "2024-05-31T08:00:00+00:00"
title = "NetworkManager 1.48"
tags = ["release"]
draft = false
author = "Fernando F. Mancera"
description = "What's new"
weight = 10
+++

A bit more than 3 months and 257 commits since NetworkManager 1.46, a new
release is ready: NetworkManager 1.48.

Let's take a look at the most interesting parts!

## Deprecate building with autotools

Building with autotools is now deprecated and will be completely removed in the
next development cycle. We recommend using meson to build NetworkManager, for
basic setup, see the
[CONTRIBUTING.md](https://gitlab.freedesktop.org/NetworkManager/NetworkManager/-/blob/main/CONTRIBUTING.md)
file.

## Deprecate `mac-address-blacklist` property

NetworkManager is embracing conscious language, as part of this work, we have
deprecated `802-11-wireless` and `802-11-wired` property
`mac-address-blacklist` in favor of `mac-address-denylist`. We encourage you to
migrate to the new property as soon as possible.

## Support changing the OpenSSL ciphers for 802.1X authentication

OpenSSL sometimes moves ciphers among SECLEVELs. Some servers are too old to
support newer ciphers so an expert should be allowed to define the ciphers for
the connection. Therefore, now it is possible to change the OpenSSL ciphers for
802.1X authentication via connection property `802-1x.openssl-ciphers`.

## What else?

Allow IPv6 SLAAC and static IPv6 DNS server assignment for modem broadband when
IPv6 device address was not explicitly passed on by ModemManager.

Support sending DHCP RELEASE message when bringing down the connection.

Support setting IPv6 preferred and valid lifetime of autogenerated temporary
addresses.

Fix a performance issue that was leading to a 100% CPU usage by NetworkManager
if external programs were doing a big amount of routes updates.

Fix detection of 6 GHz band capability for WiFi devices.

## Acknowledgements

Many thanks to all contributors who provided feedback, ideas or patches.

Alex Henrie, Beniamino Galvani, Christian K, emintufan, Fernando Fernandez
Mancera, Gris Ge, Íñigo Huguet, Jan Vaclav, Javier Sánchez Parra, jtux270,
Lubomir Rintel, Luna Jernberg, Pavel Valach, Sergey Koshelenko, Stanislas Faye,
Thomas Haller, Till Maas, Tomas Ebenlendr, Tomasz Kłoczko, Wen Liang, Yegor
Yefremov.

Also thanks to our Quality Engineers from Red Hat for all the testing:
Vladimír Beneš, Filip Pokryvka, David Jasa and Matej Berezny.

Join us on our [GitLab project](https://gitlab.freedesktop.org/NetworkManager/NetworkManager/).

## Get the new release

As usual, the next release of your favorite Linux distribution will surely
ship the new version.

In case you're too impatient to wait, or you are, in fact, responsible
for keeping NetworkManager up to date in a distribution, get the tarball
from our [download page](https://download.gnome.org/sources/NetworkManager/1.48/).

Thanks for tuning in and goodbye!
