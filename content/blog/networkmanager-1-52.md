+++
date = "2025-02-28T08:00:00+00:00"
title = "NetworkManager 1.52"
tags = ["release"]
draft = false
author = "Íñigo Huguet"
description = "What's new"
weight = 10
+++

Almost 5 months and 308 commits after the release of NetworkManager 1.50,
the new version NetworkManager 1.52 has been released.

Let's see what's new in this new release.

## Autotools is no longer supported

Autotools had been deprecated in favour of meson in NetworkManager 1.48. Now it has been
removed entirely, so meson is the only supported build system to build the project.

## Support for IPVLAN devices

IPVLAN devices are similar to MACVLAN and used in similar scenarios, Now they can be created
and managed from NetworkManager directly.

## Slowly advancing towards IPv6-only

IPv4 is not dead yet, and maybe it will never be, but the support towards IPv6-only
networks advances. In this case, NetworkManager's DHCP client can now honor the
"IPv6-only preferred" option from DHCPv4. Users willing to use IPv6 only whenever
is possible can enable the `ipv4.dhcp-ipv6-only-preferred` property for it.

"IPv6-only preferred" is a DHCPv4 option that DHCP clients can use to negotiate with
the DHCP server about using only IPv6. It needs to be enabled both in the client and
the server, ensuring that IPv4 will still be used if any of them cannot honor this
setting.

## Routed DNS

Enabling the `ipv4.routed-dns` and `ipv6.routed-dns` in a device's connection will
instruct NetworkManager to configure a special routing to make that each name
server will only be reached via the device that specifies it. Without routed DNS
the DNS queries are sent via any connected device.

## DNS over TLS

Continuing with the improvements in security on DNS, NetworkManager now supports
specifying DNS servers with DNS over TLS (DoT). This is in its initial stage of
development and currently is only supported using the dnsconfd backend, a new
daemon to manage the system's DNS configuration. Check the
[NetworkManager.conf man page](https://networkmanager.dev/docs/api/1.52.0/NetworkManager.conf.html)
to see how to configure it.

## Support for Oracle cloud (OCI) in nm-cloud-setup

The nm-cloud-setup tool, the tool used in cloud environments to automatically
apply the network configuration from the cloud provider, now supports the
OCI provider.

## What else?

- Added a new `ipv4.link-local=fallback` property to set an IPv4 link-local
  address only if no other address is set.
- Support for ethtool FEC.
- Allow to configure the DHCP addresses range in "shared" connections.
- Drop support for the "dhcpcanon" DHCP client.
- Added the "shared" method to the IPv6 configuration options in nmtui.
- Many other small changes and bugfixes.

## Acknowledgements

Many thanks to all contributors who provided feedback, ideas or patches.

Amelia Miner, Andika Triwidada, Andreas Hartmann, Beniamino Galvani,
Dan Williams, Dominique Martinet, eaglegai, Fernando Fernandez Mancera,
Filip Pokryvka, Georg Müller, Gris Ge, Íñigo Huguet, Jan Vaclav,
Jason A. Donenfeld, Jonathan Lebon, Josef Ouano, Lubomir Rintel,
Luna D Dragon, Martin von Gagern, Richard Acayan, Roman Pavelka, Till Maas,
Tim Sabsch, Tomas Korbar, Valentin Blot, Vladimír Beneš, Wen Lian and
Yuki Inoguchi.

Also thanks to our Quality Engineers from Red Hat for all the testing:
Vladimír Beneš, Filip Pokryvka and Mingyu Shi.

Join us on our [GitLab project](https://gitlab.freedesktop.org/NetworkManager/NetworkManager/).

## Get the new release

As usual, the next release of your favorite Linux distribution will surely
ship the new version.

In case you're too impatient to wait, or you are, in fact, responsible
for keeping NetworkManager up to date in a distribution, get the tarball
from our [download page](https://gitlab.freedesktop.org/NetworkManager/NetworkManager/-/releases).
(IMPORTANT: as we announced in the mailing list the download page has
changed its location and now the tarballs are released directly in our
Gitlab repository).

Thanks for tuning in and goodbye!
