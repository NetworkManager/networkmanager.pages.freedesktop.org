+++
date = "2021-02-02T12:00:00+01:00"
title = "Initscripts’ ifcfg-rh Format in NetworkManager and its Future [link]"
tags = ["ifcfg-rh", "initscripts"]
draft = false
author = "Thomas Haller"
description = "Let’s take a look at the history and future of the ifcfg-rh format"
weight = 10
+++

Let’s take a look at the history and future of the ifcfg-rh format.

Read more on [Thomas Haller's Blog](https://blogs.gnome.org/thaller/2021/02/02/initscripts-ifcfg-rh-format-in-networkmanager-and-its-future/).
