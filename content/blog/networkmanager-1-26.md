+++
date = "2020-07-13T12:00:00+01:00"
title = "NetworkManager 1.26 [link]"
tags = ["release", "cloud", "ethtool", "nmcli"]
draft = false
author = "Thomas Haller"
description = "What's new"
weight = 10
+++

A new NetworkManager version 1.26.0 was released today.

Read more on [Thomas Haller's Blog](https://blogs.gnome.org/thaller/2020/07/13/networkmanager-1-26-0/).
