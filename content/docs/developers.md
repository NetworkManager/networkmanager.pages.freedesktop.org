---
title: "Resources for developers"
date:
weight: 5
summary: ""
description: ""
bref: ""
---

### D-Bus API

NetworkManager provides a D-Bus interface on the system bus. You can
use this interface to query network state and the details of network
interfaces like current IP addresses or DHCP options, and to activate,
deactivate, created, edit, and delete saved network connections.

Check the D-Bus API reference:
{{<label style="focus" text="STABLE" link="/docs/api/latest/spec.html">}}
{{<label style="error" text="DEVEL" link="https://networkmanager.pages.freedesktop.org/NetworkManager/NetworkManager/spec.html">}}

### libnm API

libnm wraps the D-Bus API in easy-to-use GObjects and is often much
simpler for glib-based applications to use.

libnm's API provides two core aspects. One is `NMConnection` and `NMSetting`
types, which help handling NetworkManager's connection profiles. The other is
`NMClient`, which is a cache of the D-Bus objects from D-Bus API.

Check the libnm API reference:
{{<label style="focus" text="STABLE" link="/docs/libnm/latest/">}}
{{<label style="error" text="DEVEL" link="https://networkmanager.pages.freedesktop.org/NetworkManager/libnm/">}}

This is a simple example on how to print existing connections in
Python using [GObject introspection](https://gi.readthedocs.io/en/latest/):

{{< highlight python3 >}}
#!/usr/bin/python3

# GObject-introspection is needed to call libnm from python
import gi
gi.require_version("NM", "1.0")
from gi.repository import NM

def print_values(setting, key, value, flags, data):
    print("     {}.{}: {}".format(setting.get_name(), key, value))

# Create the client object. This automatically loads all the D-Bus
# tree and creates in-memory objects for connections, devices, access
# points, etc.
client = NM.Client.new(None)

# Obtain a list of connection profiles ...
connections = client.get_connections()

# ... and print their properties
for c in connections:
    print("{}:".format(c.get_id()))
    c.for_each_setting_value(print_values, None)
    print("\n")

{{< / highlight >}}

### Examples

[Examples in Python](https://gitlab.freedesktop.org/NetworkManager/NetworkManager/-/tree/main/examples/python/gi)
and [other languages](https://gitlab.freedesktop.org/NetworkManager/NetworkManager/-/tree/main/examples)
are available in the NetworkManager git tree.

### Release Numbering

Major NetworkManager releases are numbered `1.y.0`, with `y` being an
even number. For example, `1.0.0`, `1.2.0`, ..., `1.18.0`.

Minor stable releases are numbered `1.y.z`, with `y` and `z` being
even numbers. For example `1.4.2, 1.18.2`.

Development snapshots from the upstream main branch are numbered `1.y.z`
with `y` being an odd number. This is the development for the upcoming
stable release `1.$((y+1)).0`. Such snapshots don't guarantee a stable API
and have less testing. Use with care.
