---
title: "man pages"
date:
weight: 6
summary: ""
description: ""
bref: ""
---

This page contains a collection of man pages for NetworkManager. For
each page there are two links: *stable* is the version in the latest
stable release of NetworkManager, while *devel* is the version from
the latest development snapshot.

&nbsp;

### Services

|  |  |
|--|--|
|NetworkManager (8) | {{<label style="focus" text="STABLE" link="/docs/api/latest/NetworkManager.html">}} {{<label style="error" text="DEVEL" link="https://networkmanager.pages.freedesktop.org/NetworkManager/NetworkManager/NetworkManager.html">}} |
| NetworkManager.conf (5) | {{<label style="focus" text="STABLE" link="/docs/api/latest/NetworkManager.conf.html">}} {{<label style="error" text="DEVEL" link="https://networkmanager.pages.freedesktop.org/NetworkManager/NetworkManager/NetworkManager.conf.html">}} |
| NetworkManager-dispatcher (8) | {{<label style="focus" text="STABLE" link="/docs/api/latest/NetworkManager-dispatcher.html">}} {{<label style="error" text="DEVEL" link="https://networkmanager.pages.freedesktop.org/NetworkManager/NetworkManager/NetworkManager-dispatcher.html">}} |
| NetworkManager-wait-online.service (8) | {{<label style="focus" text="STABLE" link="/docs/api/latest/NetworkManager-wait-online.service.html">}} {{<label style="error" text="DEVEL" link="https://networkmanager.pages.freedesktop.org/NetworkManager/NetworkManager/NetworkManager-wait-online.service.html">}} |

&nbsp;

### Clients and Utilities

|  |  |
|--|--|
| nmcli (1) | {{<label style="focus" text="STABLE" link="/docs/api/latest/nmcli.html">}} {{<label style="error" text="DEVEL" link="https://networkmanager.pages.freedesktop.org/NetworkManager/NetworkManager/nmcli.html">}} |
| nmcli-examples (5) | {{<label style="focus" text="STABLE" link="/docs/api/latest/nmcli-examples.html">}} {{<label style="error" text="DEVEL" link="https://networkmanager.pages.freedesktop.org/NetworkManager/NetworkManager/nmcli-examples.html">}} |
| nmtui (1) | {{<label style="focus" text="STABLE" link="/docs/api/latest/nmtui.html">}} {{<label style="error" text="DEVEL" link="https://networkmanager.pages.freedesktop.org/NetworkManager/NetworkManager/nmtui.html">}} |
| nm-online (1) | {{<label style="focus" text="STABLE" link="/docs/api/latest/nm-online.html">}} {{<label style="error" text="DEVEL" link="https://networkmanager.pages.freedesktop.org/NetworkManager/NetworkManager/nm-online.html">}} |
| nm-cloud-setup (8) | {{<label style="focus" text="STABLE" link="/docs/api/latest/nm-cloud-setup.html">}} {{<label style="error" text="DEVEL" link="https://networkmanager.pages.freedesktop.org/NetworkManager/NetworkManager/nm-cloud-setup.html">}} |
| nm-initrd-generator (8) | {{<label style="focus" text="STABLE" link="/docs/api/latest/nm-initrd-generator.html">}} {{<label style="error" text="DEVEL" link="https://networkmanager.pages.freedesktop.org/NetworkManager/NetworkManager/nm-initrd-generator.html">}} |
| nm-openvswitch (7) | {{<label style="focus" text="STABLE" link="/docs/api/latest/nm-openvswitch.html">}} {{<label style="error" text="DEVEL" link="https://networkmanager.pages.freedesktop.org/NetworkManager/NetworkManager/nm-openvswitch.html">}} |

&nbsp;

### Settings

Each connection profile contains multiple settings (*connection*,
*ipv4*, *ipv6*, *ethernet*, etc.) and each setting contains
properties.

The following pages describe them and how they are represented in
different contexts, including nmcli, D-Bus and the two connection file
formats. *keyfile* is the native NetworkManager format, while
*ifcfg-rh* is the format compatible with Fedora/RHEL initscripts.

|  |  |
|--|--|
| nm-settings-nmcli (5) |  {{<label style="focus" text="STABLE" link="/docs/api/latest/nm-settings-nmcli.html">}} {{<label style="error" text="DEVEL" link="https://networkmanager.pages.freedesktop.org/NetworkManager/NetworkManager/nm-settings-nmcli.html">}} |
| nm-settings-dbus (5) | {{<label style="focus" text="STABLE" link="/docs/api/latest/nm-settings-dbus.html">}} {{<label style="error" text="DEVEL" link="https://networkmanager.pages.freedesktop.org/NetworkManager/NetworkManager/nm-settings-dbus.html">}} |
| nm-settings-keyfile (5) | {{<label style="focus" text="STABLE" link="/docs/api/latest/nm-settings-keyfile.html">}} {{<label style="error" text="DEVEL" link="https://networkmanager.pages.freedesktop.org/NetworkManager/NetworkManager/nm-settings-keyfile.html">}} |
| nm-settings-ifcfg-rh (5) | {{<label style="focus" text="STABLE" link="/docs/api/latest/nm-settings-ifcfg-rh.html">}} {{<label style="error" text="DEVEL" link="https://networkmanager.pages.freedesktop.org/NetworkManager/NetworkManager/nm-settings-ifcfg-rh.html">}} |

